package com.yangkai.model;

import javax.persistence.*;
import java.util.Date;

/**
 * 用户
 * T_USER
 * 
 * @author bianj
 * @version 1.0.0 2017-07-19
 */
@Entity
@Table(name = "T_USER", schema = "WAEPM")
public class User extends BaseEntity implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 6516463672581300868L;

    /** 数据唯一性ID */
    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "USERID", unique = true, nullable = false, length = 50)
    private String userid;

    /** 登录名 */
    @Column(name = "LOGINNAME", nullable = false, length = 100)
    private String loginname;

    /** 旧密码 */
    @Column(name = "OLDPWD", nullable = true, length = 20)
    private String oldpwd;

    /** 密码 */
    @Column(name = "PASSWORD", nullable = true, length = 20)
    private String password;

    /** 真实姓名 */
    @Column(name = "USERNAME", nullable = true, length = 20)
    private String username;

    /** 用户类型 */
    @Column(name = "USERTYPE", nullable = true, length = 20)
    private String usertype;

    /** 电话 */
    @Column(name = "TEL", nullable = true, length = 20)
    private String tel;

    /** Email */
    @Column(name = "EMAIL", nullable = true, length = 100)
    private String email;

    /** 地址 */
    @Column(name = "LOCALADDRESS", nullable = true, length = 65535)
    private String localaddress;

    /** 描述 */
    @Column(name = "REMARK", nullable = true, length = 65535)
    private String remark;

    /** 最后登录时间 */
    @Column(name = "LASTLOGINDATE", nullable = true)
    private Date lastlogindate;

    /** 创建时间 */
    @Column(name = "CREATETIME", nullable = true)
    private Date createtime;

    /** 附件地址 */
    @Column(name = "FJDZ", nullable = true, length = 65535)
    private String fjdz;

    /** 身份证/企业证号 */
    @Column(name = "IDCARD", nullable = true, length = 100)
    private String idcard;

    /** 创建时间 */
    @Column(name = "CREATEDATE", nullable = true)
    private Date createdate;

    /** 创建者 */
    @Column(name = "CREATEUSER", nullable = true, length = 50)
    private String createuser;

    /** 删除标识 0未删除 1已删除  */
    @Column(name = "DELFLAG", nullable = true, length = 10)
    private Integer delflag=0;

    /**
     * 获取数据唯一性ID
     * 
     * @return 数据唯一性ID
     */
    public String getUserid() {
        return this.userid;
    }

    /**
     * 设置数据唯一性ID
     * 
     * @param userid
     *          数据唯一性ID
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * 获取登录名
     * 
     * @return 登录名
     */
    public String getLoginname() {
        return this.loginname;
    }

    /**
     * 设置登录名
     * 
     * @param loginname
     *          登录名
     */
    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    /**
     * 获取旧密码
     * 
     * @return 旧密码
     */
    public String getOldpwd() {
        return this.oldpwd;
    }

    /**
     * 设置旧密码
     * 
     * @param oldpwd
     *          旧密码
     */
    public void setOldpwd(String oldpwd) {
        this.oldpwd = oldpwd;
    }

    /**
     * 获取密码
     * 
     * @return 密码
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * 设置密码
     * 
     * @param password
     *          密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取真实姓名
     * 
     * @return 真实姓名
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * 设置真实姓名
     * 
     * @param username
     *          真实姓名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取用户类型
     * 
     * @return 用户类型
     */
    public String getUsertype() {
        return this.usertype;
    }

    /**
     * 设置用户类型
     * 
     * @param usertype
     *          用户类型
     */
    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    /**
     * 获取电话
     * 
     * @return 电话
     */
    public String getTel() {
        return this.tel;
    }

    /**
     * 设置电话
     * 
     * @param tel
     *          电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 获取Email
     * 
     * @return Email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * 设置Email
     * 
     * @param email
     *          Email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取地址
     * 
     * @return 地址
     */
    public String getLocaladdress() {
        return this.localaddress;
    }

    /**
     * 设置地址
     * 
     * @param localaddress
     *          地址
     */
    public void setLocaladdress(String localaddress) {
        this.localaddress = localaddress;
    }

    /**
     * 获取描述
     * 
     * @return 描述
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * 设置描述
     * 
     * @param remark
     *          描述
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取最后登录时间
     * 
     * @return 最后登录时间
     */
    public Date getLastlogindate() {
        return this.lastlogindate;
    }

    /**
     * 设置最后登录时间
     * 
     * @param lastlogindate
     *          最后登录时间
     */
    public void setLastlogindate(Date lastlogindate) {
        this.lastlogindate = lastlogindate;
    }

    /**
     * 获取创建时间
     * 
     * @return 创建时间
     */
    public Date getCreatetime() {
        return this.createtime;
    }

    /**
     * 设置创建时间
     * 
     * @param createtime
     *          创建时间
     */
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /**
     * 获取附件地址
     * 
     * @return 附件地址
     */
    public String getFjdz() {
        return this.fjdz;
    }

    /**
     * 设置附件地址
     * 
     * @param fjdz
     *          附件地址
     */
    public void setFjdz(String fjdz) {
        this.fjdz = fjdz;
    }

    /**
     * 获取身份证/企业证号
     * 
     * @return 身份证/企业证号
     */
    public String getIdcard() {
        return this.idcard;
    }

    /**
     * 设置身份证/企业证号
     * 
     * @param idcard
     *          身份证/企业证号
     */
    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    /**
     * 获取创建时间
     * 
     * @return 创建时间
     */
    public Date getCreatedate() {
        return this.createdate;
    }

    /**
     * 设置创建时间
     * 
     * @param createdate
     *          创建时间
     */
    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    /**
     * 获取创建者
     * 
     * @return 创建者
     */
    public String getCreateuser() {
        return this.createuser;
    }

    /**
     * 设置创建者
     * 
     * @param createuser
     *          创建者
     */
    public void setCreateuser(String createuser) {
        this.createuser = createuser;
    }

    /**
     * 获取删除标识 0未删除 1已删除 
     * 
     * @return 删除标识 0未删除 1已删除 
     */
    public Integer getDelflag() {
        return this.delflag;
    }

    /**
     * 设置删除标识 0未删除 1已删除 
     * 
     * @param delflag
     *          删除标识 0未删除 1已删除 
     */
    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }
}