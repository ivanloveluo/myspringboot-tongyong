package com.yangkai.model;


import com.yangkai.utils.Page;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lenovo on 2015/10/14.
 */
public class BaseEntity   implements Serializable{
    @Transient
    private Page page;
    @Transient
    private String cxtj;
    //@Transient
    //private int currentPages=1; // 当前页
    //@Transient
    //private int pageSizes=5; // 每页条数
    @Transient
    private Map<String,Object> map = new HashMap<String,Object>();
    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getCxtj() {
        return cxtj;
    }

    public void setCxtj(String cxtj) {
        this.cxtj = cxtj;
    }


    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }
}
