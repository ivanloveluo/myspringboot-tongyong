package com.yangkai.mapper;

import com.yangkai.model.User;
import com.yangkai.utils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 用户
 * Created by lenovo on 2017/7/17.
 */
@Mapper
public interface UserMapper extends MyMapper<User> {

    /**
     * 带分页的用户列表查询
     * @param user
     * @return
     */
    List<User> listPageUserList(User user);

}
