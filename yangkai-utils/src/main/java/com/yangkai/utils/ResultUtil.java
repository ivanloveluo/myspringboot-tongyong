package com.yangkai.utils;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * 自定义响应结构
 * Created by lenovo on 2017/7/15.
 */
public class ResultUtil {
    //定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();
    // 响应业务状态
    private Integer code;

    // 响应消息
    private String msg;

    // 响应中的数据
    private Object data;

    public static ResultUtil build(Integer code, String msg, Object data) {
        return new ResultUtil(code, msg, data);
    }

    public static ResultUtil build(Integer code, String msg) {
        return new ResultUtil(code, msg, null);
    }

    public ResultUtil(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResultUtil(Object data) {
        this.data = data;
        this.code = 200;
        this.msg = "OK";
    }

    public ResultUtil() {
    }

    public static ResultUtil ok() {
        return new ResultUtil(null);
    }

    public static ResultUtil ok(Object data) {
        return new ResultUtil(data);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    /**
     * 将json结果集转化为TaotaoResult对象
     *
     * @param jsonData
     * @param clazz
     * @return
     */
    public static Object formatToPojo(String jsonData, Class<?> clazz) {
        try {
            ResultUtil resultUtil = ResultUtil.format(jsonData);
            if (clazz == null) {
                return ResultUtil.format(jsonData);
            } else {
                Object obj = null;
                if (resultUtil.getData() != null) {
                    obj = JSON.parseObject(JSON.toJSON(resultUtil.getData()).toString(), clazz);
                }
                return obj;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 没有object对象的转化
     *
     * @param json
     * @return
     */
    public static ResultUtil format(String json) {
        try {
            return JSON.parseObject(json, ResultUtil.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Object是集合转化
     *
     * @param jsonData json数据
     * @param clazz    集合中的类型
     * @return
     */
    public static ResultUtil formatToList(String jsonData, Class<?> clazz) {
        try {
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (data.isArray() && data.size() > 0) {
                obj = MAPPER.readValue(data.traverse(),
                        MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
            }
            return build(jsonNode.get("code").intValue(), jsonNode.get("msg").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) throws Exception {

    }
}

