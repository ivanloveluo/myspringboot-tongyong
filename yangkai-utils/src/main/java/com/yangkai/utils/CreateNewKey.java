package com.yangkai.utils;

import java.util.UUID;

public class CreateNewKey {

	 private CreateNewKey(){}
	public static synchronized String createId() {
		return  UUID.randomUUID().toString();
	}
}