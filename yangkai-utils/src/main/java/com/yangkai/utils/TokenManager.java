package com.yangkai.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by 11900 on 2019/1/19.
 */
@Component
public class TokenManager {
    @Autowired
    public RedisUtil redisUtil;
    /**
     * 创建token
     * @return
     */
    public String getToken(Object object){
        //使用uuid作为源token
        String token = UUID.randomUUID().toString().replace("-", "");
        redisUtil.set(token,object,Const.SESSION_TIME);
        return token;
    }

    /**
     * 刷新用户
     * @param token
     */
    public void refreshObjToken(String token,Object object){
        if(redisUtil.hasKey(token)){
            redisUtil.set(token,object,Const.SESSION_TIME);
        }
    }

    /**
     * 用户退出登陆
     * @param token
     */
    public void loginOff(String token){
        redisUtil.del(token);
    }

    /**
     * 获取用户信息
     * @param token
     * @return
     */
    public Object getObjByToken(String token){
        if(redisUtil.hasKey(token)){
            return redisUtil.get(token);
        }
        return null;
    }
}
