package com.yangkai.utils;

import java.lang.annotation.*;

/**
 * Created by 11900 on 2019/1/18.
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthIgnore {
}
