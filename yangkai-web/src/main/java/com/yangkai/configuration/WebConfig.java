package com.yangkai.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by 11900 on 2018/8/3.
 */


@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    public MyInterceptor myInterceptor;
       /**
     * 不需要登录拦截的url:登录注册和验证码
     */
    final String[] notLoginInterceptPaths = {"/signin","/login/**","/index/**","/register/**","/kaptcha.jpg/**","/kaptcha/**"};

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
         // 登录拦截器
         registry.addInterceptor(myInterceptor).addPathPatterns("/**");
    }

}
