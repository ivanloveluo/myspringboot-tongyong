package com.yangkai.configuration;

import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.internal.org.apache.commons.lang3.StringUtils;
import com.yangkai.controller.BaseController;
import com.yangkai.utils.AuthIgnore;
import com.yangkai.utils.Const;
import com.yangkai.utils.RedisUtil;
import com.yangkai.utils.ResultUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by 11900 on 2018/8/3.
 */
@Component
public class MyInterceptor implements HandlerInterceptor {
    Logger logger = LoggerFactory.getLogger(MyInterceptor.class);
    @Autowired
    public RedisUtil redisUtil;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        // TODO Auto-generated method stub
        logger.info("------preHandle------");
        AuthIgnore annotation;
      /*  String basePath = request.getContextPath();
        String path = request.getRequestURI();
        if(!doLoginInterceptor(path, basePath) ){//是否进行登陆拦截
            return true;
        }*/
        if(handler instanceof HandlerMethod) {
            annotation = ((HandlerMethod) handler).getMethodAnnotation(AuthIgnore.class);
        }else{
            return true;
        }

        //如果有@AuthIgnore注解，则不验证token
        if(annotation != null){
            return true;
        }

        //获取用户凭证
        String token = request.getHeader(Const.USER_TOKEN);
        if(StringUtils.isBlank(token)){
            token = request.getParameter(Const.USER_TOKEN);
        }
        if(StringUtils.isBlank(token)){
            Object obj = request.getAttribute(Const.USER_TOKEN);
            if(null!=obj){
                token=obj.toString();
            }
        }
        //请求中的token不为空时判断缓存里是不是还存在此token
        if(StringUtils.isNoneBlank(token)){
            boolean resFlag=redisUtil.hasKey(token);
            if (!resFlag){
                BaseController baseController =new BaseController();
                Object o = JSONObject.toJSON(ResultUtil.build(110, "请先登录!"));
                baseController.responseJson(o.toString(),response);
            }
           return resFlag;
        }
        //token凭证为空
        if(StringUtils.isBlank(token)){
            BaseController baseController =new BaseController();
            baseController.responseJson(JSONObject.toJSONString(ResultUtil.build(110,"请先登录!")),response);
            return false;
        }

        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // TODO Auto-generated method stub

    }
    /**
     * 是否进行登陆过滤
     * @param path
     * @param basePath
     * @return
     */
    private boolean doLoginInterceptor(String path,String basePath){
        path = path.substring(basePath.length());
        Set<String> notLoginPaths = new HashSet<>();
        //设置不进行登录拦截的路径：登录注册和验证码
        notLoginPaths.add("/user/login");
        //notLoginPaths.add("/user/getUser");
        notLoginPaths.add("/user/getUsers");
        if(notLoginPaths.contains(path)) return false;
        return true;
    }
}
