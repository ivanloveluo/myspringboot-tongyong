package com.yangkai.controller;

import com.yangkai.model.User;
import com.yangkai.service.UserService;
import com.yangkai.utils.AuthIgnore;
import com.yangkai.utils.Const;
import com.yangkai.utils.ResultUtil;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 用户
 * Created by lenovo on 2017/7/17.
 */
@Controller
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;


    /**
     * 基础数据添加校验
     * @return
     */
    @RequestMapping(value = "/checkUser",method = RequestMethod.GET)
    @ResponseBody
    public ResultUtil checkUser(User user){
        return userService.getUserCheck(user);
    }

    /**
     * 用户保存
     * @param user
     * @return
     */
    @RequestMapping(value = "/saveUser",method =  {RequestMethod.GET})
    @ResponseBody
    public ResultUtil saveUser(User user){
        user.setLoginname("lxwssss");
        user.setCreatedate(new Date());
        ResultUtil resultUtil = userService.saveUser(user);
        return resultUtil;
    }
    /**
     * 显示用户列表
     * @return
     */
    @RequestMapping(value="/userList", method =  {RequestMethod.POST,RequestMethod.GET})
    public ModelAndView userList(User user)throws Exception{
        ModelAndView mv = new ModelAndView();
        if (StringUtils.isNotBlank(user.getCxtj())){
            mv.addObject("cxtj",user.getCxtj());
            user.setCxtj(" and userName like '%"+user.getCxtj()+"%'");
        }
        List<User> list = this.userService.listPageUserList(user);
        mv.addObject("userList", list);
        mv.addObject("user", user);
        mv.setViewName("system/user_list");
        return mv;
    }
    
    /**
     * 修改或者查看某个基础数据信息
     * @return
     */
    @RequestMapping(value="/updateOrUser", method =  {RequestMethod.GET})
    @ResponseBody
    public ResultUtil updateOrUser(User user)throws Exception{
        List<User>  list = userService.getUserList(user);
        if (list!=null&&list.size()>0) {
            return ResultUtil.ok(list.get(0));
        }
        return ResultUtil.build(400,"数据提取失败!");
    }
    /**
     * 删除某个基础数据信息
     */
    @RequestMapping(value="/delUser", method =  {RequestMethod.POST})
    @ResponseBody
    public ResultUtil  delUser(@Param("userid") String userid)throws Exception{
        if(StringUtils.isNotBlank(userid)){
            return userService.delUser(userid);
        }
        return  ResultUtil.build(400,"数据提取失败!");
    }

     @RequestMapping(value = "/getUser", method = {RequestMethod.POST,
                RequestMethod.GET})
     @ResponseBody
     @AuthIgnore
     public ResultUtil getUser(HttpServletRequest request) throws Exception{
         System.out.println(request.getParameter("username")+"------------");
         User user = new User();
         super.requetToObject(request,user);
         List<User>  list = userService.getUserList(user);
         if (list!=null&&list.size()>0) {
             return ResultUtil.ok(list);
         }
         return ResultUtil.build(400,"数据提取失败!");
        }
    @RequestMapping(value = "/test", method = {RequestMethod.POST,
            RequestMethod.GET})
    @ResponseBody
    @AuthIgnore
    public ResultUtil  view(Map<String, Object> map) {
        map.put("name", "SpringBoot");
        map.put("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        redisUtil.set("test11",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        return ResultUtil.ok(map);
    }

    /**
     * 显示用户列表
     * @return
     */
    @RequestMapping(value="/getUserList", method =  {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ResultUtil getUserList(HttpServletRequest request)throws Exception{
        User user = new User();
        super.requetToObject(request,user);
        if (StringUtils.isNotBlank(user.getCxtj())){
            user.setCxtj(" and userName like '%"+user.getCxtj()+"%'");
        }
        List<User> list = this.userService.listPageUserList(user);
        if(!list.isEmpty()){
          return   ResultUtil.ok(list);
        }
        return  ResultUtil.build(400,"数据提取失败!");
    }

    /**
     * 用户登录
     * @return
     */
    @RequestMapping(value = "/login",method =  {RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    @AuthIgnore
    public ResultUtil login(HttpServletRequest request){
        String name = request.getParameter("name");
        String password = request.getParameter("password");
         if (StringUtils.isNotBlank(name)&&StringUtils.isNoneBlank(password)){
          User user = new User();
          user.setLoginname(name);
          user.setDelflag(0);
            user = userService.getUser(user);
            if (user!=null){
                if (password.equals(user.getPassword())){
                    //用户保存到session中
                    Map<String, Object> map= new HashedMap();
                    map.put(Const.USER_TOKEN,tokenManager.getToken(user));
                    return ResultUtil.ok(map);
                }else {
                    return ResultUtil.build(404,"密码错误!");
                }
            }
        }
        return ResultUtil.build(404,"用户不存在!");
    }
    /**
     * 用户退出
     * @return
     */
    @RequestMapping(value = "/logOut",method =  {RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    @AuthIgnore
    public void logOut(){
        super.tokenManager.loginOff(super.getReqToken());
    }
    /**
     * 用户保存
     * @return
     */
    @RequestMapping(value = "/saveUsers",method =  {RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    public ResultUtil saveUsers(HttpServletRequest request)throws Exception{
        User user = new User();
        super.requetToObject(request,user);
        ResultUtil resultUtil = userService.saveUser(user);
        return resultUtil;
    }
    @RequestMapping(value = "/getUsers", method = {RequestMethod.POST,
            RequestMethod.GET})
    @ResponseBody
    public ResultUtil getUsers(HttpServletRequest request) throws Exception{
      return   super.setObjectPages(User.class,request)//将requst中的属性自动封装给分页类Page
              .setListPage(userService,"listPageUserList");//调用相关服务的listpage方法
    }
}
