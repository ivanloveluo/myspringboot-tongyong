package com.yangkai.controller;

import com.xiaoleilu.hutool.convert.ConverterRegistry;
import com.xiaoleilu.hutool.util.ReflectUtil;
import com.yangkai.model.BaseEntity;
import com.yangkai.model.User;
import com.yangkai.utils.*;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

@Controller
@RequestMapping("/base")
public class BaseController {
    @Autowired
    public RedisUtil redisUtil;
    @Autowired
    public TokenManager tokenManager;
    Object object=null;
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder
            .getRequestAttributes();
        return attrs.getRequest();
    }
    // 将相关数据放入model
    protected void initResult(Model model, List<Object> list,
                              Map<String, Object> map) {
        model.addAttribute("list", list);
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry m = (Map.Entry) it.next();
            model.addAttribute(m.getKey().toString(), m.getValue());
        }
    }

    /**
     * 得到请求中的token
     * @return
     */
    public String getReqToken() {
        String token = getRequest().getHeader(Const.USER_TOKEN);
        if(StringUtils.isBlank(token)){
            token = getRequest() .getParameter(Const.USER_TOKEN);
        }
       return token;
    }
    /**
     * 获取用户信息
     *
     * @return
     */
    public User getUserByToken() {
        String token = getReqToken();
        if (StringUtils.isNotBlank(token)){
               return (User) tokenManager.getObjByToken(token);
         }
         return null;
    }
    /**
     * 显示信息发布信息列表
     *
     * @return
     */
    @RequestMapping(value = "/gotoJsp", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView gotoJsp(HttpServletRequest request ) throws Exception {
        ModelAndView mv = new ModelAndView();
        String jsp = request.getParameter("jsp_");
        mv.setViewName(jsp);
        return mv;
    }
    /**
     * 得到request中的请求参数
     *
     * @param request
     * @return
     */
    public Map getQueryMap(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        Enumeration enu = request.getParameterNames();
        while (enu.hasMoreElements()) {
            String paraName = (String) enu.nextElement();
            if (StringUtils.isNotBlank(request.getParameter(paraName))) {
                map.put(paraName, request.getParameter(paraName).trim());
            }
        }
        return map;
    }
    /**
     * 将requst中的属性自动封装给相应的对象
     *
     * @param request
     * @param object
     * @throws Exception
     */
    public Object requetToObject(HttpServletRequest request, Object object)
            throws Exception {
        Map<String, Object> queryMap = this.getQueryMap(request);
        ConvertUtils.register(new DateTimeConverter(), java.util.Date.class); // 在封装之前
        // 注册转换器
        BeanUtils.populate(object, queryMap);// 方法的作用：用来将一些 key-value 的值（例如
        // hashmap）映射到 bean 中的属性。
        return object;
    }
    /**
     * 下载
     *
     * @param file
     * @param title
     * @param response
     * @throws Exception
     */
    public void responseDownload(File file, String title,
                                 HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        if (file != null) {
            // 下载
            BufferedOutputStream bos = null;
            BufferedInputStream bis = null;
            try {
                long fileLength = file.length();
                response.setContentType("application/x-download");
                request.setCharacterEncoding("UTF-8");
                response.setHeader("Content-Length", String.valueOf(fileLength));
                response.addHeader("Content-Disposition",
                    "attachment;filename="
                        + new String(title.getBytes(), "ISO8859-1")
                        + ".xls"); // 设置文件下载类型
                if (file.exists()) {// 文件是否存在
                    bis = new BufferedInputStream(new FileInputStream(file));
                    bos = new BufferedOutputStream(response.getOutputStream());
                    int bytesRead = 0;
                    byte[] buffer = new byte[2048];
                    while (-1 != (bytesRead = bis
                        .read(buffer, 0, buffer.length))) {
                        bos.write(buffer, 0, bytesRead);
                    }
                }
            } catch (IOException e) {
                response.reset();
            } finally {
                try {
                    if (bos != null) {
                        bos.close();
                    }
                    if (bis != null) {
                        bis.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            file.delete();
        }
    }

    public void downFile(HttpServletRequest request, HttpServletResponse response, File file) {
        if (file != null) {
            //下载
            BufferedOutputStream bos = null;
            BufferedInputStream bis = null;
            try {
                long fileLength = file.length();
                String agent = request.getHeader("User-Agent");
                response.setContentType("application/x-download");
                request.setCharacterEncoding("UTF-8");
                response.setHeader("Content-Length", String.valueOf(fileLength));
                boolean isMsie = (agent != null && agent.indexOf("MSIE") != -1);
                if (isMsie) {
                    response.addHeader("Content-Disposition", "MemberAttch;filename=" + URLEncoder.encode(file.getName(), "UTF-8"));
                    ; //设置文件下载类型
                } else {
                    response.addHeader("Content-Disposition", "MemberAttch;filename=" + new String(file.getName().getBytes("UTF-8"), "ISO-8859-1"));
                    ; //设置文件下载类型
                }
                if (file.exists()) {//文件是否存在
                    bis = new BufferedInputStream(new FileInputStream(file));
                    bos = new BufferedOutputStream(response.getOutputStream());
                    int bytesRead = 0;
                    byte[] buffer = new byte[2048];
                    while (-1 != (bytesRead = bis.read(buffer, 0, buffer.length))) {
                        bos.write(buffer, 0, bytesRead);
                    }
                }
            } catch (IOException e) {
                response.reset();
            } finally {
                try {
                    if (bos != null) {
                        bos.close();
                    }
                    if (bis != null) {
                        bis.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void responseJson(String json, HttpServletResponse response)
        throws Exception {
        response.setContentType("application/json;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.getWriter().write(json);
        response.getWriter().flush();
        response.getWriter().close();
    }

    public void responseText(String json, HttpServletResponse response)
        throws Exception {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Cache-Control", "no-cache, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.getWriter().write(json);
        response.getWriter().flush();
        response.getWriter().close();
    }

    /**
     * 获取用户IP地址
     *
     * @param request
     * @return
     */
    public String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader(" x-forwarded-for ");
        if (ip == null || ip.length() == 0 || " unknown ".equalsIgnoreCase(ip)) {
            ip = request.getHeader(" Proxy-Client-IP ");
        }
        if (ip == null || ip.length() == 0 || " unknown ".equalsIgnoreCase(ip)) {
            ip = request.getHeader(" WL-Proxy-Client-IP ");
        }
        if (ip == null || ip.length() == 0 || " unknown ".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }


    /**
     * 根据返回的int类型，转为为boolean
     *
     * @param count 多少行受影响
     * @return flag
     * @author Hank (jiangbo@firstelite.net)
     * @since 2015年9月21日
     */
    public boolean getBoolean(Integer count) {

        boolean flag = false;
        if (count == 0) {
            flag = false;
        } else {
            flag = true;
        }
        return flag;
    }

    /**
     * 将requst中的属性自动封装给相应的对象
     *
     * @param o
     * @throws Exception
     */
    public Object setObjectPage(Class o, HttpServletRequest request)
            throws Exception {
        Object o1 = o.newInstance();
        Object page = ReflectHelper.getFieldValue(o1, "page");
        if (page==null){
            page = Page.class.newInstance();
            ReflectHelper.setFieldValue(o1,"page",page);
            if (StringUtils.isNotBlank(request.getParameter("currentPage"))){
                ReflectHelper.setFieldValue(page, "currentPage", ConverterRegistry.getInstance().convert(Integer.class, request.getParameter("currentPage")));
                           }else {
                ReflectHelper.setFieldValue(page, "currentPage",1);
            }
        }
        return o1;
    }

    /**
     * 将requst中的属性自动封装给分页类Page
     * 链式调用
     * @param o
     * @throws Exception
     */
    public BaseController setObjectPages(Class o, HttpServletRequest request)
            throws Exception {
        object = o.newInstance();
        Object page = ReflectHelper.getFieldValue(object, "page");
        if (page==null){
            page = Page.class.newInstance();
            ReflectHelper.setFieldValue(object,"page",page);
            if (StringUtils.isNotBlank(request.getParameter("currentPage"))){
                ReflectHelper.setFieldValue(page, "currentPage", ConverterRegistry.getInstance().convert(Integer.class, request.getParameter("currentPage")));
            }else {
                ReflectHelper.setFieldValue(page, "currentPage",1);
            }
        }
        this .requetToObject2(request);//将requst中的属性自动封装给相应的对象
        return this;//链式调用
    }
    /**
     * 将requst中的属性自动封装给相应的对象
     *
     * @param request
     * @throws Exception
     */
    public BaseController requetToObject2(HttpServletRequest request)
            throws Exception {
        Map<String, Object> queryMap = this.getQueryMap(request);
        ConvertUtils.register(new DateTimeConverter(), java.util.Date.class); // 在封装之前
        // 注册转换器
        BeanUtils.populate(object, queryMap);// 方法的作用：用来将一些 key-value 的值（例如
        // hashmap）映射到 bean 中的属性。
        return this;//链式调用
    }

    /**
     * 调用相关服务的listpage方法
     * @param servers userService
     * @param method listPageUserList
     * @return
     * @throws Exception
     */
    public ResultUtil setListPage(Object servers, String method)
            throws Exception {
        List listsPage =  ReflectUtil.invoke(servers, method, object);
        Map<String,Object> map = new HashedMap();
        map.put("page",ReflectHelper.getFieldValue(object,"page"));
        map.put("list",listsPage);
        if (listsPage!=null&&listsPage.size()>0) {
            return ResultUtil.ok(map);
        }
        return ResultUtil.build(400,"数据提取失败!");
    }
    public BaseController isNotPage(BaseEntity baseEntity){
        object = baseEntity;
        if (baseEntity.getPage()==null){
            baseEntity.setPage(new Page());
        }
        return this;
    }
}