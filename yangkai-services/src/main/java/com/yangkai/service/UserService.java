package com.yangkai.service;

import com.yangkai.model.User;
import com.yangkai.utils.ResultUtil;

import java.util.List;
import java.util.Map;

/**
 * Created by lenovo on 2017/7/19.
 */
public interface UserService {

    ResultUtil getUserCheck(User user);

    ResultUtil saveUser(User user) ;

    List<User> listPageUserList(User user);


    List<User> getUserList(User user) throws Exception;

    ResultUtil delUser(String id);

    User getUser(User user);

    Map<String,Object> getMenu(User user, String parentId);
}
