package com.yangkai.service.imp;

import com.yangkai.mapper.UserMapper;
import com.yangkai.model.User;
import com.yangkai.service.UserService;
import com.yangkai.utils.CreateNewKey;
import com.yangkai.utils.ResultUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lenovo
 * @date 2017/7/19
 */
@Service
public class UserServiceImple implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
        public ResultUtil getUserCheck(User user) {
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(user.getUserid())) {
            criteria.andNotEqualTo("userid", user.getUserid());
        }
        if (StringUtils.isNotBlank(user.getLoginname())) {
            criteria.andEqualTo("loginname", user.getLoginname());
        }
        criteria.andEqualTo("delflag","0");
        List<User> object = userMapper.selectByExample(example);
        return ResultUtil.ok(object.size());
    }

    @Override
    public ResultUtil saveUser(User user) {
        try {
            if (StringUtils.isNotBlank(user.getUserid())) {
                userMapper.updateByPrimaryKeySelective(user);
            } else {
                user.setUserid(CreateNewKey.createId());
                user.setCreatedate(new Date());
                int i = userMapper.insertSelective(user);
                System.out.println(i+"--------insertSelective----");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultUtil.build(500, "保存失败!");
        }
        return ResultUtil.build(200, "保存成功!");
    }

    @Override
    public List<User> listPageUserList(User user) {
        return userMapper.listPageUserList(user);
    }

    @Override
    public List<User> getUserList(User user) throws Exception{
        user.setDelflag(0);
        try {
            int a =5/0;
        } catch (Exception e) {
            System.out.println(e);
        }
        return userMapper.select(user);
    }

    @Override
    public ResultUtil delUser(String id) {
        if(StringUtils.isNotBlank(id)){
            User user = userMapper.selectByPrimaryKey(id);
            user.setDelflag(1);
            int i = userMapper.updateByPrimaryKey(user);
            if (i==1){
                return ResultUtil.build(200, "删除成功!");
            }
        }
        return ResultUtil.build(500, "删除失败!");
    }

    @Override
    public User getUser(User user) {
        user = userMapper.selectOne(user);
        return user;
    }

    @Override
    public Map<String, Object> getMenu(User user, String parentId) { ;
        return null;
    }

}
