
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `USERID` varchar(50) NOT NULL COMMENT '数据唯一性ID',
  `LOGINNAME` varchar(100) NOT NULL COMMENT '登录名',
  `OLDPWD` varchar(20) DEFAULT NULL COMMENT '旧密码',
  `PASSWORD` varchar(20) DEFAULT NULL COMMENT '密码',
  `USERNAME` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `USERTYPE` varchar(20) DEFAULT NULL COMMENT '用户类型',
  `TEL` varchar(20) DEFAULT NULL COMMENT '电话',
  `EMAIL` varchar(100) DEFAULT NULL COMMENT 'Email',
  `LOCALADDRESS` text COMMENT '地址',
  `REMARK` text COMMENT '描述',
  `LASTLOGINDATE` datetime DEFAULT NULL COMMENT '最后登录时间',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `FJDZ` text COMMENT '附件地址',
  `IDCARD` varchar(100) DEFAULT NULL COMMENT '身份证/企业证号',
  `CREATEDATE` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATEUSER` varchar(50) DEFAULT NULL COMMENT '创建者',
  `DELFLAG` int(65) DEFAULT NULL COMMENT '删除标识 0未删除 1已删除 ',
  PRIMARY KEY (`USERID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', null, '1', 'admin', 'JG', '15291967131', null, '西安交易所', null, '2016-04-11 20:06:06', null, 'd://user_reg\\admin\\', null, null, '1', '0');
INSERT INTO `t_user` VALUES ('10d12225-799d-4112-a41e-e5a614edfc76', 'xijie@xbcq.com', null, '1', '惠杰', 'JG', null, null, null, null, '2016-03-09 13:58:52', '2016-02-22 13:18:30', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('13f5144d-8151-40fe-ae34-90d8c254370f', 'geren@qq.com', null, '1', '个人', 'GR', null, null, null, null, '2016-02-23 14:33:42', '2015-12-11 14:19:51', 'd://user_reg\\李四\\', null, null, '13f5144d-8151-40fe-ae34-90d8c254370f', '0');
INSERT INTO `t_user` VALUES ('20388170-7e75-4df0-bacd-ff218fc0af20', 'aaa@qq.com', null, '1', null, 'GR', null, null, null, null, null, '2016-03-02 15:12:34', null, null, null, null, '1');
INSERT INTO `t_user` VALUES ('29229395-7442-43e8-9333-cb1e29d6bd74', 'jys@qq.com', null, '1', '交易所用户', 'JG', null, null, null, null, null, '2016-01-09 15:36:05', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('2bfda056-8989-4fd4-a008-24cb6173a9ba', 'xiangtj@126.com', null, '1', null, 'GR', '1234567', '119009626@qq.com', null, null, '2016-01-29 08:55:24', '2016-01-19 17:05:45', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('2fab11ff-200c-4992-ad15-c4435302ce00', 'Piero@126.com', null, '1', '皮靉 罗', 'GR', '13709291024', null, 'Uventus', null, '2016-03-07 03:12:59', '2016-03-06 23:03:09', 'd://user_reg\\皮靉 罗\\', null, null, '2fab11ff-200c-4992-ad15-c4435302ce00', '0');
INSERT INTO `t_user` VALUES ('2ff308f2-fede-43be-ad0e-3db2874a0200', 'g', null, null, 'f', 'JG', null, null, null, null, null, null, null, null, '2018-12-25 09:53:58', null, '0');
INSERT INTO `t_user` VALUES ('30ce9303-6a1c-40cb-bcc0-d8e74e6c1b36', 'Vandersa@qq.com', null, '1', '范德萨', 'GR', '13319231923', null, '要', null, '2016-03-06 23:25:23', '2016-03-06 20:12:04', 'd://user_reg\\范德萨\\', null, null, '30ce9303-6a1c-40cb-bcc0-d8e74e6c1b36', '0');
INSERT INTO `t_user` VALUES ('346b34c8-47fa-4293-a579-f25ca8537a5', 'qiye@qq.com', null, '1', '企业', 'QY', '13288672314', null, '测试机构', null, '2016-03-08 09:11:08', '2015-12-27 14:27:03', 'd://user_reg\\企业\\', '010', null, '346b34c8-47fa-4293-a579-f25ca8537a5', '0');
INSERT INTO `t_user` VALUES ('3911220e-620e-48fc-bae6-33b9338c4420', 'mawh@xbcq.com', null, '1', '马伟红', 'JG', null, null, null, null, null, '2016-01-12 15:27:39', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('422c0743-b694-4aaf-886e-163725b8b68d', 'gerenwj@qq.com', null, '1', '王娟）', 'GR', '13809991234', null, '11111', null, '2016-03-06 14:28:25', '2016-03-06 14:20:36', 'd://user_reg\\王娟）\\', null, null, '422c0743-b694-4aaf-886e-163725b8b68d', '0');
INSERT INTO `t_user` VALUES ('47e761ce-be43-4827-b4cf-e0e0111671e0', 'wangchen@xbcq.com', null, '1', '王琛', 'JG', null, null, null, null, '2016-03-09 16:43:41', '2016-01-12 14:45:39', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('4b05ca88-a7f5-4538-85dc-d122c592a0bc', 'laizhe@xbcq.com', null, '1', '来哲', 'JG', null, null, null, null, '2016-03-09 17:02:15', '2016-02-22 13:27:53', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('4dd74cac-fbc5-42fb-8ca1-925c26f98b92', '2@qq.com', null, '1', '张三', 'GR', null, null, null, null, '2016-03-04 13:52:33', '2015-12-11 14:21:11', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('58c00706-5908-4260-94b4-df38f239713f', '124@qq.com', null, '1', null, 'QY', null, null, null, null, '2016-03-07 00:58:33', '2016-02-28 19:01:46', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('5d2eaaae-9ef8-49c2-9a55-d1e7e2bec93a', 'yangln@xbcq.com', null, '1', '杨莉娜', 'JG', null, null, null, null, '2016-03-07 03:38:31', '2016-01-12 14:49:58', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('633fd5f7-3151-4c3e-a8de-3bca282e807e', 'jjzcr@qq.com', null, '1', null, 'GR', null, null, null, null, '2015-12-13 16:04:33', '2015-12-13 16:02:31', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('6797509c-842c-47a5-898d-71b47e9a1622', 'yangwn@xbcq.com', null, '1', '杨渭娜', 'JG', null, null, null, null, '2016-03-03 16:53:34', '2016-01-12 14:49:22', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('6d928c04-e112-462a-adeb-90f8d1754478', 'VanPercy@qq.com', null, '1', null, 'QY', null, null, null, null, '2016-02-29 20:09:38', '2016-02-29 17:48:54', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('7456b470-7224-4210-bc4e-f6cb8cff196f', 'lxw@qq.com', null, '1', '罗伟', 'QY', '13256567759', null, '陕西西安', null, '2016-03-04 14:26:18', '2016-03-03 14:20:36', 'd://user_reg\\罗伟\\', '1123', null, '7456b470-7224-4210-bc4e-f6cb8cff196f', '0');
INSERT INTO `t_user` VALUES ('75b151dc-7963-465d-a939-97934915f85a', 'becker@126.com', null, '1', null, 'QY', null, null, null, null, '2016-03-07 00:56:01', '2016-02-28 20:20:19', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('78552602-bd6c-4ca5-a650-a4e4ad71475f', 'qy@qq.com', null, '1', null, 'QY', null, null, null, null, null, '2015-12-27 14:30:49', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('7db0925c-72ae-4f5b-9a86-62a12af74724', 'gg@qq.com', null, '1', null, 'QY', null, null, null, null, '2016-01-15 17:24:54', '2015-12-27 14:59:41', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('82d49020-be06-4eeb-a322-f755005e7a38', '3@qq.com', null, '1', '王五', 'GR', null, null, null, null, null, '2015-12-11 14:20:13', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('8759e5ac-cd32-4acc-8eb6-80d9a987fbd7', 'owen@126.com', null, '1', '1', 'QY', '13909194305', null, '1', null, '2016-03-06 23:26:29', '2016-03-06 21:17:39', 'd://user_reg\\1\\', '1', null, '8759e5ac-cd32-4acc-8eb6-80d9a987fbd7', '0');
INSERT INTO `t_user` VALUES ('8e9f661d-e693-420e-9841-d00bf28c11ac', 'qy@qq.com', null, '1', null, 'QY', null, null, null, null, null, '2015-12-27 14:30:41', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('9514ff83-ebe2-4245-bca2-cbeeb5d6cac6', 'wangjun@xbcq.com', null, '1', '王军', 'JG', null, null, null, null, '2016-03-09 16:32:58', '2016-02-22 16:32:04', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('98a6bfdc-073a-413e-a575-a81f4253691f', 'huhj@xbcq.com', null, '1', '胡海娟', 'JG', null, null, null, null, '2016-03-09 16:26:40', '2016-01-12 14:46:46', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('995e1208-15de-42c2-a327-5989b2ca8666', 'huangxc@xbcq.com', null, '1', '黄晓春', 'JG', null, null, null, null, '2016-03-06 20:06:03', '2016-02-22 13:20:22', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('9db51af7-4802-4645-ac4c-24786f86950f', 'ganzm@xbcq.com', null, '1', '甘枝梅', 'JG', null, null, null, null, '2016-03-09 16:33:36', '2016-01-12 14:47:21', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('a5a00750-cb92-466f-85b5-c57a5c620da0', 'leizong@xbcq.com', null, '1', '雷总', 'JG', null, null, null, null, '2016-03-06 19:17:00', '2016-03-06 19:15:27', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('af9ef01c-2074-4816-8f32-d5cf37d34111', '55@qq.com', null, '1', '测试会员', 'QY', '15298985567', null, '陕西西安', null, '2016-03-09 16:12:47', '2015-12-27 14:53:05', 'd://user_reg\\测试会员\\', 'sa007', null, 'af9ef01c-2074-4816-8f32-d5cf37d34111', '0');
INSERT INTO `t_user` VALUES ('b35315d8-3394-453e-ab3c-285e4b07cd19', 'wangjf@xbcq.com', null, '1', '王俊峰', 'JG', null, null, null, null, '2016-03-09 16:33:15', '2016-01-12 14:48:16', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('b910ffca-a064-4d72-a389-025ca2548aa4', 'xbcq@xbcq.com', null, '1', null, 'GR', null, null, null, null, null, '2016-01-14 14:21:34', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('ba2ac196-c998-4459-bbdc-7c4474ac37cc', 'qy-mwh@qq.com', null, '1', 'qy-mwh', 'QY', '13759922528', null, '长安北路', null, '2016-03-09 17:00:01', '2016-03-04 15:20:09', 'd://user_reg\\qy-mwh\\', '111111', null, 'ba2ac196-c998-4459-bbdc-7c4474ac37cc', '0');
INSERT INTO `t_user` VALUES ('bf2d9a9e-4dd3-475f-8068-6bda99535c72', 'wangjuan@xbcq.com', null, '1', '王娟', 'JG', null, null, null, null, '2016-02-23 09:11:28', '2016-01-12 14:50:35', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('c29f73f4-3318-4d25-b178-29877004af05', '823208298@qq.com', null, '1', '吉祥', 'GR', '13333333333', null, '陕西省西安市碑林区长安北路14号、陕西省政务大厅', null, '2016-03-07 10:58:09', '2016-03-07 10:50:13', 'd://user_reg\\吉祥\\', null, null, 'c29f73f4-3318-4d25-b178-29877004af05', '0');
INSERT INTO `t_user` VALUES ('c8825169-e3e2-468b-8e44-97962a6113b7', 'ruantao@xbcq.com', null, '1', '阮涛', 'JG', null, null, null, null, '2016-03-09 16:30:55', '2016-01-12 14:47:57', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('c995a50f-4dc9-488b-9375-e15b44a11601', 'beck@126.com', null, '1', null, 'QY', null, null, null, null, '2016-03-07 00:19:35', '2016-02-27 15:12:38', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('e2beaefb-8477-4e8a-9819-713cee0dfead', '123456@qq.com', null, '1', '李小昭', 'QY', '13809134567', null, '中国陕西省西安市', null, '2016-03-09 14:32:55', '2016-03-02 15:23:32', 'd://user_reg\\李小昭\\', '71357515-4', null, 'e2beaefb-8477-4e8a-9819-713cee0dfead', '0');
INSERT INTO `t_user` VALUES ('e86f1dc8-4787-4901-bf55-22c4b5394557', '1222@qq.com', null, '1', null, 'GR', null, null, null, null, '2016-03-04 18:22:06', '2016-03-02 15:22:51', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('e94ecf0f-544d-45e7-a3c1-b79e33827c2a', 'cuizy@xbcq.com', null, '1', '崔泽阳', 'JG', null, null, null, null, '2016-03-09 13:55:13', '2016-02-22 13:26:01', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('e94f8d8d-7564-4fe6-a2ab-930f91f29608', 'hanxt@xbcq.com', null, '1', '韩晓婷', 'JG', null, null, null, null, null, '2016-01-12 14:48:38', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('f6455cef-c739-4d48-aad9-7ffcdba22e86', 'ptyh01@qq.com', null, '1', null, 'QY', null, null, null, null, '2016-01-16 14:32:55', '2016-01-16 13:28:58', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('f7be0d4a-934e-462c-9b3a-5a716b3ec68c', '123@qq.com', null, '1', null, 'GR', null, null, null, null, '2016-03-07 03:12:21', '2016-02-28 16:19:40', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('f870188b-2903-4537-93ad-976cfaeb3433', '113@qq.com', null, '1', null, 'QY', null, null, null, null, '2016-03-02 14:27:26', '2016-03-02 14:25:53', null, null, null, null, '0');
INSERT INTO `t_user` VALUES ('f9025a9c-b9b1-4ee0-82d2-d994ebfc5d5f', 'totti@126.com', null, '1', 'Totti', 'GR', '13809194305', null, 'abc@MU', null, '2016-03-07 02:27:25', '2016-02-29 17:36:52', 'd://user_reg\\Totti\\', null, null, 'f9025a9c-b9b1-4ee0-82d2-d994ebfc5d5f', '1');
INSERT INTO `t_user` VALUES ('feb44d88-6e3e-4ee2-94d7-a31b57ab95d8', 'xiaoxq@xbcq.com', null, '1', '肖秀琴', 'JG', null, null, null, null, '2016-02-21 16:30:10', '2016-01-12 14:46:21', null, null, null, null, '0');
